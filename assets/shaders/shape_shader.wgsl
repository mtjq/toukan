struct VertexInput {
    @location(0) position: vec4<f32>,
    @location(1) colour: vec4<f32>,
};

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) colour: vec4<f32>,
};

// @group(0) @binding(0)
// var<uniform> pos: vec3<f32>;

@vertex
fn vs_main(
    vertex_in: VertexInput,
) -> VertexOutput {
    var out: VertexOutput;
    // out.clip_position = in.position + vec4<f32>(pos, 1.0);
    out.clip_position = vertex_in.position;
    out.colour = vertex_in.colour;
    return out;
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    return in.colour;
}
