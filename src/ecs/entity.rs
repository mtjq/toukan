use crate::ecs::component::*;


// -----------------------------------------------------------------------------
// type EntityId
// -----------------------------------------------------------------------------
pub type EntityId = usize;

pub fn generate_entity_id() -> EntityId {
  static mut NEXT_UID: EntityId = 0;
  // TODO: not thread safe.
  unsafe {
    if NEXT_UID == EntityId::MAX {
      panic!("Cannot create more entities, uids reached the max.");
    }
    NEXT_UID += 1;
    NEXT_UID -1
  }
}


// -----------------------------------------------------------------------------
// struct Entity
// -----------------------------------------------------------------------------
#[derive(Debug)]
pub struct Entity {
  id: EntityId,
  components: u64,
}

impl Entity {
  pub fn new() -> Entity {
    Entity {
      id: generate_entity_id(),
      components: 0,
    }
  }

  pub fn id(&self) -> &EntityId {
    &self.id
  }

  pub fn add_component(&mut self, component_id: &ComponentId) {
    self.components |= 1 << *component_id;
  }
}


// -----------------------------------------------------------------------------
// struct WorldEntities
// -----------------------------------------------------------------------------
#[derive(Default)]
pub struct WorldEntities {
  entities: Vec<Entity>,
  count: usize,
}

impl WorldEntities {
  pub fn new() -> WorldEntities {
    WorldEntities {
      entities: Vec::<Entity>::new(),
      count: 0,
    }
  }

  pub fn new_entity(&mut self, entity: Entity) -> EntityId {
    self.entities.push(entity);
    self.count += 1;
    *self.entities[self.count - 1].id()
  }

  pub fn add_component_to_entity(
    &mut self,
    component_id: &ComponentId,
    entity_id: &EntityId,
  ) {
    self.entities[*entity_id].add_component(component_id);
  }

  pub fn count(&self) -> usize {
    self.count
  }
}
