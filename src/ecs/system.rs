use std::any::TypeId;
use crate::ecs::component::*;


// -----------------------------------------------------------------------------
// struct SimpleSystem
// -----------------------------------------------------------------------------
pub struct SimpleSystem<T: Component> {
  func: fn(&mut T),
  component_type: TypeId,
}

impl<T: Component> SimpleSystem<T> {
  pub fn new(func: fn(&mut T)) -> SimpleSystem<T> {
    SimpleSystem {
      func,
      component_type: TypeId::of::<T>(),
    }
  }
}


// -----------------------------------------------------------------------------
// trait System
// -----------------------------------------------------------------------------
pub trait System {
  fn run(&self, components: &mut WorldComponents);
  fn single_run(&self, component: &mut dyn Component);
  fn component_type(&self) -> TypeId;
}

impl<T: Component> System for SimpleSystem<T> {
  fn run(&self, components: &mut WorldComponents) {
    components.fetch_components::<T>()
      .data_mut()
      .into_iter()
      .for_each(|component| self.single_run(&mut **component));
  }

  fn single_run(&self, component: &mut dyn Component) {
    (self.func) (component.downcast_mut::<T>().unwrap());
  }

  fn component_type(&self) -> TypeId {
    self.component_type
  }
}


// -----------------------------------------------------------------------------
// struct Systems
// -----------------------------------------------------------------------------
#[derive(Default)]
pub struct Systems {
  systems: Vec<Box<dyn System>>,
}

impl Systems {
  pub fn add_system<T: Component>(&mut self, system: SimpleSystem<T>) {
    self.systems.push(Box::new(system) as Box<dyn System>);
  }

  pub fn run(&self, world_components: &mut WorldComponents) {
    self.systems.iter().for_each(|system| system.run(world_components));
  }
}
