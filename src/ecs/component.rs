use std::{
  any::TypeId,
  collections::HashMap,
  fmt::Debug,
};
use downcast_rs::{
  Downcast,
  impl_downcast,
};

use crate::ecs::entity::*;


pub type ComponentId = u32;
static MAX_COMPONENTS:u32 = 64; // Flags on a u64.


// -----------------------------------------------------------------------------
// trait Component
// -----------------------------------------------------------------------------
// TODO: derive macro.
pub trait Component: Debug + Downcast {}
impl_downcast!(Component);


// -----------------------------------------------------------------------------
// struct ComponentIds
// -----------------------------------------------------------------------------
#[derive(Debug, Default)]
pub struct ComponentIds {
  ids: HashMap<TypeId, ComponentId>, // TODO: anymap.
}

impl ComponentIds {
  pub fn new() -> ComponentIds {
    ComponentIds::default()
  }

  // TODO: use Result.
  pub fn add_component<T: Component + 'static>(&mut self) -> &ComponentId {
    if self.ids.len() as u32 >= MAX_COMPONENTS {
      panic!("Can not add any more components to the world.");
    }

    let new_id = self.ids.len() as ComponentId;
    self.ids.entry(TypeId::of::<T>()).or_insert(new_id)
  }

  pub fn get_id<T: Component>(&self) -> Option<&ComponentId> {
    match self.ids.get(&TypeId::of::<T>()) {
      Some(id) => Some(id as &ComponentId),
      None => None
    }
  }
}


// -----------------------------------------------------------------------------
// struct ComponentInstances
// -----------------------------------------------------------------------------
#[derive(Debug)]
pub struct ComponentInstances<T: Component> {
  data: Vec<Box<T>>,
  entity_to_components: HashMap<EntityId, usize>,
}

impl<T: Component> ComponentInstances<T> {
  pub fn new() -> ComponentInstances<T> {
    ComponentInstances {
      data: Vec::<Box<T>>::new(),
      entity_to_components: HashMap::<EntityId, usize>::new(),
    }
  }

  // TODO: make it a method that returns the iter of data.
  pub fn data(&self) -> &Vec::<Box<T>> {
    &self.data
  }

  // TODO: make it a method that returns the mut iter of data.
  pub fn data_mut(&mut self) -> &mut Vec::<Box<T>> {
    &mut self.data
  }
}


// -----------------------------------------------------------------------------
// trait ComponentInstancesTrait
// -----------------------------------------------------------------------------
pub trait ComponentInstancesTrait: Debug + Downcast {
  fn insert_component(
    &mut self,
    entity_id: &EntityId,
    component: Box<dyn Component>
  );
}
impl_downcast!(ComponentInstancesTrait);

impl<T: Component> ComponentInstancesTrait for ComponentInstances<T> {
  fn insert_component(
    &mut self,
    entity_id: &EntityId,
    component: Box<dyn Component>
  ) {
    self.data.push(component.downcast::<T>().unwrap());
    self.entity_to_components.insert(*entity_id, self.data.len() - 1);
  }
}


// -----------------------------------------------------------------------------
// struct WorldComponents
// -----------------------------------------------------------------------------
#[derive(Default)]
pub struct WorldComponents {
  components: Vec<Box<dyn ComponentInstancesTrait>>,
  ids: ComponentIds,
}

impl WorldComponents {
  pub fn new() -> WorldComponents {
    WorldComponents::default()
  }

  pub fn add_component_to_entity<T: Component + 'static>(
    &mut self,
    component: T,
    entity_id: &EntityId
  ) -> &ComponentId {
    // TODO: borrow checker doesn't handle early exit. Find a better workaround.
    match self.ids.get_id::<T>() {
      Some(_) => {
        let id = self.ids.get_id::<T>().unwrap();
        self.components[*id as usize].insert_component(
          entity_id,
          Box::new(component)
        );
        id
      }
      None => {
        let id = self.ids.add_component::<T>();
        let mut component_instance = Box::new(ComponentInstances::<T>::new());
        component_instance.insert_component(entity_id, Box::new(component));
        self.components.push(component_instance);
        id
      }
    }
  }

  pub fn fetch_components<T: Component>(
    &mut self,
  ) -> &mut ComponentInstances<T> {
    let component_id = self.ids.get_id::<T>()
      .expect("Could not fetch components for given TypeId.");

    (&mut *self.components[*component_id as usize])
      .downcast_mut::<ComponentInstances<T>>()
      .unwrap()
  }
}
