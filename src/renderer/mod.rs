// use std::borrow::Cow;
use glam::*;
// use winit::window::Window;
// use wgpu::{
//   BufferDescriptor,
//   util::DeviceExt,
// };

// use crate::app::App;

pub mod mesh;


pub struct Renderer<'w> {
  surface: wgpu::Surface<'w>,
  device: wgpu::Device,
  queue: wgpu::Queue,
  config: wgpu::SurfaceConfiguration,
  player_render_pipeline: wgpu::RenderPipeline,
  player_vertex_buffer: wgpu::Buffer,
  player_index_buffer: wgpu::Buffer,
  player_indices_count: u32,
  player_instance_buffer: wgpu::Buffer,
  player_instances_count: u32,
  shape_render_pipeline: wgpu::RenderPipeline,
  shape_vertex_buffer: wgpu::Buffer,
  shape_index_buffer: wgpu::Buffer,
  shape_indices_count: u32,
  // pos_buffer: wgpu::Buffer,
  // pos_bind_group: wgpu::BindGroup,
}

/*
impl<'w> Renderer<'w> {
  pub async fn new(
    window: &'w Window, app: &App
  ) -> Renderer<'w> {
    let mut size = window.inner_size();
    size.width = size.width.max(1);
    size.height = size.height.max(1);

    let instance = wgpu::Instance::default();

    let surface = instance.create_surface(window).unwrap();
    let adapter = instance
      .request_adapter(&wgpu::RequestAdapterOptions {
        power_preference: wgpu::PowerPreference::default(),
        force_fallback_adapter: false,
        compatible_surface: Some(&surface),
      })
      .await
      .expect("Failed to find an appropriate adapter.");

    let (device, queue) = adapter
      .request_device(
        &wgpu::DeviceDescriptor {
          label: None,
          required_features: wgpu::Features::empty(),
          required_limits: wgpu::Limits::downlevel_defaults()
            .using_resolution(adapter.limits()),
        },
        None,
      )
      .await
      .expect("Failed to create device.");

    let config = surface
      .get_default_config(&adapter, size.width, size.height)
      .expect("Unable to get default config for the surface.");
    surface.configure(&device, &config);

    let swapchain_capabilities = surface.get_capabilities(&adapter);
    let swapchain_format = swapchain_capabilities.formats[0];

    // Players
    let player_shader = device.create_shader_module(
      wgpu::ShaderModuleDescriptor {
        label: Some("Simple triangle shader"),
        source: wgpu::ShaderSource::Wgsl(
          // TODO
          Cow::Borrowed(include_str!("../../assets/shaders/player_shader.wgsl"))
        ),
      }
    );

    let player_vertex_buffer = device.create_buffer_init(
      &wgpu::util::BufferInitDescriptor {
        label: Some("Player vertex buffer"),
        contents: app.player1.mesh().vertex_buffer(),
        usage: wgpu::BufferUsages::VERTEX,
      }
    );

    let player_index_buffer = device.create_buffer_init(
      &wgpu::util::BufferInitDescriptor {
        label: Some("Player index buffer"),
        contents: app.player1.mesh().index_buffer(),
        usage: wgpu::BufferUsages::INDEX,
      }
    );

    let player_instance_buffer = device.create_buffer(
      &BufferDescriptor {
        label: Some("Player instance buffer"),
        size: 2 * app.player1.mesh().instance_buffer_size(),
        usage: wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
        mapped_at_creation: false,
      }
    );

    let player_vertex_layout = wgpu::VertexBufferLayout {
      array_stride: std::mem::size_of::<mesh::Vertex>() as wgpu::BufferAddress,
      step_mode: wgpu::VertexStepMode::Vertex,
      attributes: &[
        wgpu::VertexAttribute {
          format: wgpu::VertexFormat::Float32x4,
          offset: 0,
          shader_location: 0,
        },
        wgpu::VertexAttribute {
          format: wgpu::VertexFormat::Float32x4,
          offset: std::mem::size_of::<[f32; 4]>() as wgpu::BufferAddress,
          shader_location: 1,
        },
      ],
    };

    let player_instance_layout = wgpu::VertexBufferLayout {
      array_stride: std::mem::size_of::<Mat4>() as wgpu::BufferAddress,
      step_mode: wgpu::VertexStepMode::Instance,
      attributes: &[
        wgpu::VertexAttribute {
          format: wgpu::VertexFormat::Float32x4,
          offset: 0,
          shader_location: 2,
        },
        wgpu::VertexAttribute {
          format: wgpu::VertexFormat::Float32x4,
          offset: std::mem::size_of::<[f32; 4]>() as wgpu::BufferAddress,
          shader_location: 3,
        },
        wgpu::VertexAttribute {
          format: wgpu::VertexFormat::Float32x4,
          offset: std::mem::size_of::<[f32; 8]>() as wgpu::BufferAddress,
          shader_location: 4,
        },
        wgpu::VertexAttribute {
          format: wgpu::VertexFormat::Float32x4,
          offset: std::mem::size_of::<[f32; 12]>() as wgpu::BufferAddress,
          shader_location: 5,
        },
      ],
    };

    let shape_vertex_layout = player_vertex_layout.clone();

    let pipeline_layout = device.create_pipeline_layout(
      &wgpu::PipelineLayoutDescriptor {
        label: None,
        // bind_group_layouts: &[&pos_bind_group_layout],
        bind_group_layouts: &[],
        push_constant_ranges: &[],
      });

    let player_render_pipeline = device.create_render_pipeline(
      &wgpu::RenderPipelineDescriptor {
        label: Some("Player render pipeline"),
        layout: Some(&pipeline_layout),
        vertex: wgpu::VertexState {
          module: &player_shader,
          entry_point: "vs_main",
          buffers: &[player_vertex_layout, player_instance_layout],
        },
        fragment: Some(wgpu::FragmentState {
          module: &player_shader,
          entry_point: "fs_main",
          targets: &[Some(swapchain_format.into())],
        }),
        primitive: wgpu::PrimitiveState::default(),
        depth_stencil: None,
        multisample: wgpu::MultisampleState::default(),
        multiview: None,
      }
    );

    // Shape
    let shape_shader = device.create_shader_module(
      wgpu::ShaderModuleDescriptor {
        label: Some("Shape shader"),
        source: wgpu::ShaderSource::Wgsl(
          Cow::Borrowed(include_str!("../../assets/shaders/shape_shader.wgsl")) // TODO
        ),
      }
    );

    let shape_vertex_buffer = device.create_buffer_init(
      &wgpu::util::BufferInitDescriptor {
        label: Some("Shape vertex buffer"),
        contents: app.shape.vertex_buffer(),
        usage: wgpu::BufferUsages::VERTEX,
      }
    );

    let shape_index_buffer = device.create_buffer_init(
      &wgpu::util::BufferInitDescriptor {
        label: Some("Shape index buffer"),
        contents: app.shape.index_buffer(),
        usage: wgpu::BufferUsages::INDEX,
      }
    );

    let shape_render_pipeline = device.create_render_pipeline(
      &wgpu::RenderPipelineDescriptor {
        label: Some("Shape render pipeline"),
        layout: Some(&pipeline_layout),
        vertex: wgpu::VertexState {
          module: &shape_shader,
          entry_point: "vs_main",
          buffers: &[shape_vertex_layout],
        },
        fragment: Some(wgpu::FragmentState {
          module: &shape_shader,
          entry_point: "fs_main",
          targets: &[Some(swapchain_format.into())],
        }),
        primitive: wgpu::PrimitiveState::default(),
        depth_stencil: None,
        multisample: wgpu::MultisampleState::default(),
        multiview: None,
      }
    );

    // let pos_buffer = device.create_buffer_init(
    //   &wgpu::util::BufferInitDescriptor {
    //     label: Some("Position buffer"),
    //     contents: bytemuck::cast_slice(&[*pos]),
    //     usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
    //   }
    // );
    //
    // let pos_bind_group_layout = device.create_bind_group_layout(
    //   &wgpu::BindGroupLayoutDescriptor {
    //     label: Some("Position bind group layout."),
    //     entries: &[
    //       wgpu::BindGroupLayoutEntry {
    //         binding: 0,
    //         visibility: wgpu::ShaderStages::VERTEX,
    //         ty: wgpu::BindingType::Buffer {
    //           ty: wgpu::BufferBindingType::Uniform,
    //           has_dynamic_offset: false,
    //           min_binding_size: None,
    //         },
    //         count: None,
    //       }
    //     ],
    //   }
    // );
    //
    // let pos_bind_group = device.create_bind_group(
    //   &wgpu::BindGroupDescriptor {
    //     label: Some("Position bind group"),
    //     layout: &pos_bind_group_layout,
    //     entries: &[
    //       wgpu::BindGroupEntry {
    //         binding: 0,
    //         resource: pos_buffer.as_entire_binding(),
    //       }
    //     ],
    //   }
    // );

    Renderer {
      surface,
      device,
      queue,
      config,
      player_render_pipeline,
      player_vertex_buffer,
      player_index_buffer,
      player_indices_count: app.player1.mesh().indices_count(),
      player_instance_buffer,
      player_instances_count: 2,
      shape_render_pipeline,
      shape_vertex_buffer,
      shape_index_buffer,
      shape_indices_count: app.shape.indices_count(),
      // pos_buffer,
      // pos_bind_group,
    }
  }

  pub fn render(&mut self, app: &App) {
    let surface_texture = self.surface
      .get_current_texture()
      .expect("Failed to get next swap chain texture.");
    let view = surface_texture.texture.create_view(
      &wgpu::TextureViewDescriptor::default());
    let mut encoder = self.device.create_command_encoder(
      &wgpu::CommandEncoderDescriptor {
        label: Some("Render Encoder"),
      });

    {
      let mut rpass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
        label: Some("Player pass"),
        color_attachments: &[Some(wgpu::RenderPassColorAttachment {
          view: &view,
          resolve_target: None,
          ops: wgpu::Operations {
            load: wgpu::LoadOp::Clear(wgpu::Color {
              r: 0.1,
              g: 0.2,
              b: 0.3,
              a: 1.0,
            }),
            store: wgpu::StoreOp::Store,
          },
        })],
        depth_stencil_attachment: None,
        occlusion_query_set: None,
        timestamp_writes: None,
      });

      // Players
      rpass.set_pipeline(&self.player_render_pipeline);
      rpass.set_vertex_buffer(0, self.player_vertex_buffer.slice(..));
      self.queue.write_buffer(&self.player_instance_buffer,
                              0,
                              app.player1.mesh().instance_buffer());
      self.queue.write_buffer(&self.player_instance_buffer,
                              app.player1.mesh().instance_buffer_size(),
                              app.player2.mesh().instance_buffer());
      rpass.set_vertex_buffer(1, self.player_instance_buffer.slice(..));
      rpass.set_index_buffer(self.player_index_buffer.slice(..),
                             wgpu::IndexFormat::Uint16);
      rpass.draw_indexed(0..self.player_indices_count,
                         0,
                         0..self.player_instances_count);

      // Shape
      rpass.set_pipeline(&self.shape_render_pipeline);
      rpass.set_vertex_buffer(0, self.shape_vertex_buffer.slice(..));
      rpass.set_index_buffer(self.shape_index_buffer.slice(..),
                             wgpu::IndexFormat::Uint16);
      // rpass.set_bind_group(0, &self.pos_bind_group, &[]);
      rpass.draw_indexed(0..self.shape_indices_count, 0, 0..1);
    }
    // self.queue.write_buffer(&self.pos_buffer, 0, bytemuck::cast_slice(&[*pos]));
    self.queue.submit(Some(encoder.finish()));
    surface_texture.present();
  }

  pub fn update_size(&mut self, new_size: &winit::dpi::PhysicalSize<u32>) {
    self.config.width = new_size.width.max(1);
    self.config.height = new_size.height.max(1);
    self.surface.configure(&self.device, &self.config);
  }
}
*/
