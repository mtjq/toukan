use glam::*;


#[repr(C)]
#[derive(Default, Copy, Clone, Debug, bytemuck::Zeroable, bytemuck::Pod)]
pub struct Vertex {
  pos: [f32; 4],
  colour: [f32; 4],
}

impl Vertex {
  pub fn new(pos: [f32; 4], colour: [f32; 4]) -> Vertex {
    Vertex { pos, colour }
  }
}


#[repr(C)]
#[derive(Default, Debug, Clone)]
pub struct SquareMesh {
  global_matrix: Mat4,
  vertices: [Vertex; 4],
  indices: [u16; 6],
}

impl SquareMesh {
  pub fn new(pos: Vec3, colours: [[f32; 4]; 4]) -> SquareMesh {
    let vertices = [
      Vertex { pos: [0.0, 0.0, 0.0, 1.0], colour: colours[0] },
      Vertex { pos: [1.0, 0.0, 0.0, 1.0], colour: colours[1] },
      Vertex { pos: [0.0, 1.0, 0.0, 1.0], colour: colours[2] },
      Vertex { pos: [1.0, 1.0, 0.0, 1.0], colour: colours[3] },
    ];
    SquareMesh {
      global_matrix: Mat4::from_translation(pos),
      vertices,
      indices: [0, 1, 2, 2, 1, 3],
    }
  }

  pub fn vertex_buffer(&self) -> &[u8] {
    bytemuck::cast_slice(&self.vertices)
  }

  pub fn index_buffer(&self) -> &[u8] {
    bytemuck::cast_slice(&self.indices)
  }

  pub fn indices_count(&self) -> u32 {
    self.indices.len() as u32
  }

  pub fn instance_buffer(&self) -> &[u8] {
    bytemuck::bytes_of(&self.global_matrix)
  }

  pub fn instance_buffer_size(&self) -> u64 {
    std::mem::size_of::<Mat4>() as u64
  }

  pub fn translate(&mut self, translation: Vec3) {
    self.global_matrix =
      Mat4::from_translation(translation) * self.global_matrix;
  }
}


#[repr(C)]
#[derive(Default, Debug, Clone)]
pub struct PolygonMesh {
  vertices: Vec<Vertex>,
  indices: Vec<u16>,
}

impl PolygonMesh {
  pub fn new(vertices: Vec<Vertex>, indices: Vec<u16>) -> PolygonMesh {
    PolygonMesh { vertices, indices }
  }

  pub fn vertex_buffer(&self) -> &[u8] {
    bytemuck::cast_slice(self.vertices.as_slice())
  }

  pub fn index_buffer(&self) -> &[u8] {
    bytemuck::cast_slice(self.indices.as_slice())
  }

  pub fn indices_count(&self) -> u32 {
    self.indices.len() as u32
  }
}
