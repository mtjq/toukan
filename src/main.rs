use toukan::{
  app::App,
  ecs::{
    component::Component,
    system::SimpleSystem,
  },
};

fn main() {
  let mut app = App::new();

  // Player is owned by app...
  let toukan_id = app.new_entity();
  let hello_toukan = HelloComponent::new("Toukan");
  app.add_component_to_entity(hello_toukan, toukan_id);
  let bye_toukan = ByeComponent::new("Toukan");
  app.add_component_to_entity(bye_toukan, toukan_id);

  let player_id = app.new_entity();
  let hello_player = HelloComponent::new("player");
  app.add_component_to_entity(hello_player, player_id);

  let hello_system = SimpleSystem::new(hello_system_fn);
  app.add_system(hello_system);
  let bye_system = SimpleSystem::new(bye_system_fn);
  app.add_system(bye_system);

  app.run();
}

#[derive(Debug, Default)]
struct HelloComponent {
  pub name: String,
}

impl HelloComponent {
  pub fn new(name: &str) -> HelloComponent {
    HelloComponent {
      name: String::from(name),
    }
  }
}

impl Component for HelloComponent {}


#[derive(Debug, Default)]
struct ByeComponent {
  pub name: String,
}

impl ByeComponent {
  pub fn new(name: &str) -> ByeComponent {
    ByeComponent {
      name: String::from(name),
    }
  }
}

impl Component for ByeComponent {}



fn hello_system_fn(hello_component: &mut HelloComponent) {
  println!("Hello {}.", hello_component.name);
}


fn bye_system_fn(bye_component: &mut ByeComponent) {
  println!("Bye {}.", bye_component.name);
}


/*
#[derive(Debug, Clone)]
pub struct Player {
  mesh: SquareMesh,
  move_left_key: KeyCode,
  move_right_key: KeyCode,
}

impl Player {
  pub fn new(
    pos: Vec3,
    colours: [[f32; 4]; 4],
    move_left_key: KeyCode,
    move_right_key: KeyCode,
  ) -> Player {
    Player {
      mesh: SquareMesh::new(pos, colours),
      move_left_key,
      move_right_key,
    }
  }

  pub fn mesh(&self) -> &SquareMesh {
    &self.mesh
  }

  pub fn translate(&mut self, translation: Vec3) {
    self.mesh.translate(translation);
  }
}

impl Entity for Player {
  fn update(&mut self, event: &Event<()>) {
    match event {
      Event::DeviceEvent {
        event: DeviceEvent::Key(RawKeyEvent {
          physical_key: PhysicalKey::Code(keycode),
          state: ElementState::Pressed
        }),
        ..
      } => match keycode {
        k if *k == self.move_left_key => self.translate(-0.2 * Vec3::X),
        k if *k == self.move_right_key => self.translate(0.2 * Vec3::X),
        _ => ()
      }
      _ => ()
    }
  }
}







    let player1 = Player::new(
      Vec3::new(-0.7, -0.7, 0.0),
      [ [0.3, 0.2, 0.7, 1.0],
        [0.3, 0.2, 0.7, 1.0],
        [0.3, 0.2, 0.7, 1.0],
        [0.3, 0.2, 0.7, 1.0],
      ],
      KeyCode::KeyS,
      KeyCode::KeyF);
    let player2 = Player::new(
      Vec3::new(0.3, 0.3, 0.0),
      [ [0.7, 0.3, 0.2, 1.0],
        [0.7, 0.3, 0.2, 1.0],
        [0.7, 0.3, 0.2, 1.0],
        [0.7, 0.3, 0.2, 1.0],
      ],
      KeyCode::KeyJ,
      KeyCode::KeyL);
    let shape = mesh::PolygonMesh::new(
      vec!(
        mesh::Vertex::new([-0.1, -0.1, 0.0, 1.0], [0.2, 0.7, 0.3, 1.0]),
        mesh::Vertex::new([ 0.1, -0.1, 0.0, 1.0], [0.2, 0.7, 0.3, 1.0]),
        mesh::Vertex::new([ 0.0,  0.1, 0.0, 1.0], [0.2, 0.7, 0.3, 1.0]),
      ),
      vec!(0, 1, 2)
    );
*/
