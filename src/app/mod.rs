use winit::{
  window::WindowBuilder,
  event_loop::{ControlFlow, EventLoop, EventLoopWindowTarget},
  event::*,
  keyboard::*,
};

// use crate::renderer::*;
use crate::ecs::{
  entity::*,
  component::*,
  system::*,
};


// -----------------------------------------------------------------------------
// struct App
// -----------------------------------------------------------------------------
#[derive(Default)]
pub struct App {
  world: World,
  systems: Systems,
  // pub player1: Player,
  // pub player2: Player,
  // pub shape: mesh::PolygonMesh,
}

impl App {
  pub fn new() -> App {
    App::default()
    // App { player1, player2, shape }
  }

  pub fn new_entity(&mut self) -> EntityId {
    self.world.add_entity()
  }

  pub fn add_component_to_entity<T: Component + 'static>(
    &mut self,
    component: T,
    entity_id: EntityId
  ) {
    self.world.add_component_to_entity(component, &entity_id);
  }

  pub fn add_system<T: Component>(&mut self, system: SimpleSystem<T>) {
    self.systems.add_system(system);
  }

  pub fn run(&mut self) {
    self.init();

    let event_loop = EventLoop::new().unwrap();
    event_loop.set_control_flow(ControlFlow::Poll);
    let window = WindowBuilder::new().build(&event_loop).unwrap();

    // let mut renderer =
    //   pollster::block_on(Renderer::new(&window, self));

    let _ = event_loop.run(|event, elwt| {
      self.run_systems(&event, elwt);
      match event {
        Event::WindowEvent { window_id, event } => match event {
          WindowEvent::RedrawRequested if window_id == window.id() => {
            // renderer.render(self);
            window.request_redraw(); // TODO: handle redraws differently.
          }
          WindowEvent::Resized(_new_size) => {
            // renderer.update_size(&new_size);
            window.request_redraw();
          }
          _ => (),
        }
        _ => (),
      }
    });
  }

  fn init(&mut self) {
    env_logger::init();
  }

  fn run_systems(&mut self, event: &Event<()>, elwt: &EventLoopWindowTarget<()>) {
    // self.player1.update(event);
    // self.player2.update(event);
    match event {
      Event::WindowEvent { window_id: _, event } => match event {
        WindowEvent::CloseRequested => elwt.exit(),
        _ => ()
      }
        Event::DeviceEvent {
        event: DeviceEvent::Key(RawKeyEvent {
          physical_key: PhysicalKey::Code(keycode),
          state: ElementState::Pressed
        }),
        ..
      } => match keycode {
        KeyCode::Escape | KeyCode::KeyY => elwt.exit(),
        _ => ()
      }
      _ => ()
    };
    self.systems.run(self.world.components());
  }
}


// -----------------------------------------------------------------------------
// struct World
// -----------------------------------------------------------------------------
#[derive(Default)]
struct World {
  entities: WorldEntities,
  components: WorldComponents,
}

impl World {
  // pub fn new() -> World {
  //   World::default()
  // }

  fn add_entity(&mut self) -> EntityId {
    self.entities.new_entity(Entity::new())
  }

  // TODO: understand 'static.
  pub fn add_component_to_entity<T: Component + 'static>(
    &mut self,
    component: T,
    entity_id: &EntityId,
  ) {
    let id = self.components.add_component_to_entity(component, entity_id);
    self.entities.add_component_to_entity(id, entity_id);
  }

  pub fn components(&mut self) -> &mut WorldComponents {
    &mut self.components
  }
}
